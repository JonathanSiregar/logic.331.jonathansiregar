package InputPlural;

import java.util.Scanner;

public class Soal04DariInternetBelumPaham {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();
        int modus = 0;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        int[] frekuensimuncul = new int[255]; //alokasi memori atau panjang index sebuah array

        for(int i = 0; i < 10; i++){ //mengisi array frekuensimuncul dengan nilai awal 0
            frekuensimuncul[i] = 0;
        }

        for (int i = 0; i < length; i++) { //tampung
            frekuensimuncul[intArray[i]]++;
        }

        for (int i = 0; i < 10; i++) { //mencari nilai modus
            if(frekuensimuncul[i] > modus){
                modus = i;
            }
        }
        System.out.println("Modus = " + modus + "Muncul sebanyak " + frekuensimuncul[modus]);
    }
}
