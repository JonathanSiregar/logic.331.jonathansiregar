package InputPlural;

import java.util.Scanner;

public class Soal06BelumSelesai {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        int counter = 0;
        for(int i = 0 ; i < length-1 ; i++ ) {
            for (int j = i + 1; j < length && intArray[i] != 0; j++) {
                if (intArray[i] == intArray[j]) {
                    counter++;
                    intArray[j] = 0; // this is just a placeholder ???????
                }
            }
            if (counter == 1)
                System.out.println("pair found for: " + intArray[i]);
            counter = 0;
        }
    }
}
