package InputPlural;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int index = -1;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
            index = i;
            for (int j = i; j <= length - 1; j++) {
                if( intArray[j] < intArray[index] ){
                    index = j;
                }
            }
            int temp = intArray[i];
            intArray[i] = intArray[index];
            intArray[index] = temp;
        }
        for (int i = 0; i < length; i++) {
            System.out.print(intArray[i] + " ");
        }
    }
}
