package InputPlural;

import WarmupLMS.Utility;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve(){// sama dengan no 9 input plural
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();
        double bilanganpositif = 0, bilangannegatif = 0, nol = 0;

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;

        for (int i = 0; i < length; i++) {
            if(intArray[i] > 0 ){
                bilanganpositif++;
            } else if (intArray[i] < 0) {
                bilangannegatif++;
            } else {
                nol++;
            }
        }
        System.out.println("Ratio bilangan positif = " + bilanganpositif/ intArray.length);
        System.out.println("Ratio bilangan negatif = " + bilangannegatif/ intArray.length);
        System.out.println("Ratio bilangna nol = " + nol/ intArray.length);
    }
}
