package Array2D;

import java.util.Scanner;

public class Soal01
{
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        int baris1 = 0;
        int baris2 = 1;
        int counter = 6;


        System.out.println("Input n");
        int n = input.nextInt();

        System.out.println("Input n2");
        int n2 = input.nextInt();

        int[][] results = new int[2][n];

        for (int i = 0; i < 2; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
                if(i == 0)
                {
                    results[i][j] = baris1;
                    baris1 ++;
                }
                else if (i == 1)
                {
                    results[i][j] = baris2;
                    int pangkat = baris1 - counter;
                    baris2 = (int) Math.pow(n2,pangkat);
                    counter -= 1;

                }
            }
        }

        Utility.PrintArray2D(results);
    }
}
