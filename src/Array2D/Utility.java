package Array2D;

public class Utility {

    public static void PrintArray2D(int[][] results)
    {
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                System.out.print(results[i][j] + " ");

            }
            System.out.println();

        }
    }
    public static void PrintMinus(int[][] results)
    {
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                if(results[i][j] == 9){
                    System.out.print("-9 ");
                }
                else if (results[i][j] == 243) {
                    System.out.print("-243 ");
                }
                else {
                    System.out.print(results[i][j] + " ");
                }
            }
            System.out.println();
        }
    }

    public static void MinusModulus3(int[][] results){
        for (int i = 0; i < results.length; i++) {
            for (int j = 0; j < results[0].length; j++) {
                if(results[i][j] == 9){
                    System.out.print("-9 ");
                }
                else if (results[i][j] == 243) {
                    System.out.print("-243 ");
                }
                else if (results[i][j] % 3 == 0) {
                    System.out.print("-" + results[i][j] + " ");
                }
                else {
                    System.out.print(results[i][j] + " ");
                }
            }
            System.out.println();
        }
    }
}
