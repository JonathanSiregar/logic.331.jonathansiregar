package Array2D;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int baris1 = 0;
        int baris2 = 0;
        int baris3 = 0;
        int kurang = 6;
        int kurang2 = 6;
        int kurang3 = 6;
        int tambah1 = 1;
        int tambah2 = 2;

        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
                if(i == 0) {
                    results[i][j] = baris1;
                    baris1 = n - kurang;
                    kurang --;
                }
                else if (i == 1) {
                    results[i][j] = baris2;
                    baris2 = (n - kurang2) +  tambah1;
                    kurang2 -- ;
                    tambah1 += 1 ;

                }
                else {
                    results[i][j] = baris3;
                    baris3 = (n - kurang3) + tambah2;
                    kurang3 -- ;
                    tambah2 ++ ;
                }
            }
        }

        Utility.PrintArray2D(results);
    }
}
