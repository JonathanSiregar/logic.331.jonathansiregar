package Array2D;

import java.util.Scanner;

public class Soal10 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        System.out.println("Input n2");
        int n2 = input.nextInt();

        int baris1 = 0;
        int baris2 = 0;
        int baris3 = 0;
        int kurang = 6;
        int kurang2 = 2*n2;
        int kurang3 = 2*n2;
        int tambah1 = 2;
        int tambah2 = 3;

        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
                if(i == 0) {
                    results[i][j] = baris1;
                    baris1 = n - kurang;
                    kurang --;
                }
                else if (i == 1) {
                    results[i][j] = baris2;
                    baris2 = (n - kurang2) +  tambah1;
                    kurang2 -- ;
                    tambah1 += 2 ;

                }
                else {
                    results[i][j] = baris3;
                    baris3 = (n - kurang3) + tambah2;
                    kurang3 -- ;
                    tambah2 += 3 ; //kurang penggunaan n2
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
