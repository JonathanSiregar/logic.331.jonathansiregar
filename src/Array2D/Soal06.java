package Array2D;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        int baris1 = 0;
        int baris2 = 1;
        int baris3 = 1 ;
        int counter = n - 1;
        int tambah = 1;

        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
                if(i == 0) {
                    results[i][j] = baris1;
                    baris1 ++;
                }
                else if (i == 1) {
                    results[i][j] = baris2;
                    baris2 *= n;

                }
                else {
                    results[i][j] = baris3;
                    int pangkat = n - counter;
                    baris3 = ((int) Math.pow(n,pangkat) + tambah);
                    counter -= 1;
                    tambah ++;
                }
            }
        }

        Utility.PrintArray2D(results);
    }
}
