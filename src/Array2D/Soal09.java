package Array2D;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();

        System.out.println("Input n2");
        int n2 = input.nextInt();

        int baris1 = 0;
        int baris2 = 0;
        int baris3 = 18;
        int deret = n;
        int counter = n - 1;
        int kurang3 = 6;
        int tambah = 2;

        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
                if(i == 0) {
                    results[i][j] = baris1;
                    baris1 ++ ;
                                    }
                else if (i == 1) {
                    results[i][j] = baris2;
                    baris2 = (deret - counter) * 3;
                    counter -= 1;

                }
                else {
                    results[i][j] = baris3;
                    baris3 = (n - kurang3) + tambah;
                    kurang3 -- ;
                    tambah ++ ;
                }
            }
        }

        Utility.PrintArray2D(results);
    }
}// kurang penggunaan n2
