package Array2D;

import java.util.Scanner;

public class Contoh {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);

        int ganjil = 1;
        int genap = 2;

        System.out.println("Input n");
        int n = input.nextInt();

        int[][] results = new int[2][n];

        for (int i = 0; i < 2; i++) {//baris   //kenapa menggunakan 2 for : menyelesaikan for j terelebih dahulu sampai kondisi false baru lanjut for 1 dan masuk ke for j lagi
            for (int j = 0; j < n; j++) {//kolom
             if(i == 0)
             {
                 results[i][j] = ganjil;
                 ganjil += 2;
                }
             else if (i == 1)
             {
                 results[i][j] = genap;
                 genap += 2;
             }
            }
        }

        Utility.PrintArray2D(results);
    }
}
