package BangunDatar;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
//       int age = 22;
//       String name = "Jonathan Eric Gideon Siregar";
//       boolean flag = true;
//
//        System.out.println("Hello World");
//        System.out.println("My name is " + name);
//        System.out.println("I am " + age + " years old") ;

//        Luas Persegi Panjang
        Scanner input = new Scanner(System.in);

        boolean flag = true;
        String answer = "y";

        while(flag){

            System.out.println("Silahkan Pilih Bangun Datar");
            System.out.println("1. Persegi panjang");
            System.out.println("2. Segitiga");
            System.out.println("3. Trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");

            int pilihan1 = input.nextInt();
            int pilihan2;

            String prompt = "Pilihan =      1. Luas       2. Keliling";

            switch (pilihan1){
                case 1 :
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        PersegiPanjang.Luas();
                    } else if (pilihan2 == 2) {
                        PersegiPanjang.Keliling();
                    }else{
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 2 :
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        Segitiga.Luas();
                    } else if (pilihan2 == 2) {
                        Segitiga.Kelilinng();
                    }else{
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 3 :
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        Trapesium.Luas();
                    } else if (pilihan2 == 2) {
                        Trapesium.Kelilinng();
                    }else{
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 4 :
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        Lingkaran.Luas();
                    } else if (pilihan2 == 2) {
                        Lingkaran.Kelilinng();
                    }else{
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 5 :
                    System.out.println(prompt);
                    pilihan2 = input.nextInt();

                    if(pilihan2 == 1){
                        Persegi.Luas();
                    } else if (pilihan2 == 2) {
                        Persegi.Keliling();
                    }else{
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                default:
                    System.out.println("Pilihan tidak tersedia");
            }

            System.out.println("Try again? y/n");
            input.nextLine(); //skip bug
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }


//        if (pilihan == 1) {
//            System.out.println("Input Panjang (cm)");
//            int panjang = input.nextInt();
//
//            System.out.println("Input Lebar (cm)");
//            int lebar = input.nextInt();
//
//            int luas = panjang * lebar;
//
//            System.out.println("Luas Persegi Panjang adalah = " + luas);
//        }
//        else if (pilihan == 2) {
////          Luas & Keliling Segitiga
//            System.out.println("Input Alas (cm)");
//            int sisialas = input.nextInt();
//
//            System.out.println("Input Tinggi (cm)");
//            int sisitinggi = input.nextInt();
//
//            System.out.println("Input Sisi Miring (cm)");
//            int sisimiring = input.nextInt();
//
//            double luassegitiga = 0.5 * (sisialas * sisitinggi);
//            int kelilingsegitiga = sisialas + sisitinggi + sisimiring;
//
//            System.out.println("Luas Segitiga = " + luassegitiga);
//            System.out.println("Keliling Segitiga = " + kelilingsegitiga);
//        }
//        else if (pilihan ==3) {
//            //       Luas & Keliling Trapesium
//            System.out.println("Input Sisi Sejajar Atas (cm)");
//            int sisipendek = input.nextInt();
//            System.out.println("Input Sisi Sejajar Bawah (cm)");
//            int sisipanjang = input.nextInt();
//            System.out.println("Input Sisi Miring 1 (cm)");
//            int sisimiring1 = input.nextInt();
//            System.out.println("Input Sisi Miring 2 (cm)");
//            int sisimiring2 = input.nextInt();
//            System.out.println("Input Sisi Tinggi (cm)");
//            int tinggitrapesium = input.nextInt();
//
//            double luastrapesium = 0.5 * (sisipendek + sisipanjang) * tinggitrapesium;
//            int kelilingtrapesium = sisipendek + sisipanjang + sisimiring1 + sisimiring2;
//
//            System.out.println("Luas trapesium = " + luastrapesium);
//            System.out.println("Keliling trapesium = " + kelilingtrapesium);
//
//        }
//        else if (pilihan == 4) {
//            //      Luas & Keliling Lingkaran
//            System.out.println("Input Jari Jari Lingkaran (cm)");
//            double radius = input.nextDouble();
//            double pi = 3.14;
//
//            double luaslingkaran = pi * radius * radius;
//            double kelilinglingkaran = 2 * pi * radius;
//
//            System.out.println("Luas Lingkaran = " + luaslingkaran);
//            System.out.println("Keliling Lingkaran = " + kelilinglingkaran);
//        }
//        else if (pilihan == 5) {
//            //      Luas & Keliling Persegi
//            System.out.println("Input Sisi Persegi (cm)");
//            int sisi = input.nextInt();
//
//            int luaspersegi = sisi * sisi;
//            int kelilingpersegi = 4 * sisi;
//
//            System.out.println("Luas Persegi = " + luaspersegi);
//            System.out.println("Keliling Persegi = " + kelilingpersegi);
//        }
//        else {
//            System.out.println("Pilihan Tidak Tersedia");
//        }

    }
}