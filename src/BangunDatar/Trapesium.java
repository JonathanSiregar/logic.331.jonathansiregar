package BangunDatar;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);
    private static int sisiatas;
    private static int sisibawah;
    private static int miring1;
    private static int miring2;
    private static int tinggi;

    public static void Luas(){
        System.out.println("Input sisi atas");
        sisiatas = input.nextInt();

        System.out.println("Input sisi bawah");
        sisibawah  = input.nextInt();

        System.out.println("Input tinggi");
        tinggi = input.nextInt();

        double luas = 0.5 * (sisiatas + sisibawah) * tinggi;
        System.out.println("Luas = " + luas);
    }

    public static void Kelilinng(){
        System.out.println("Input sisi atas");
        sisiatas = input.nextInt();

        System.out.println("Input sisi bawah");
        sisibawah  = input.nextInt();

        System.out.println("Input Sisi Miring 1");
        miring1 = input.nextInt();

        System.out.println("Input Sisi Miring 2");
        miring2 = input.nextInt();

        int keliling = sisiatas + sisibawah + miring1 + miring2;
        System.out.println("Keliling = " + keliling);
    }
}
