package BangunDatar;

import java.util.Scanner;

public class Segitiga {
    private static Scanner input = new Scanner(System.in);
    private static int alas;
    private static int tinggi;
    private static int miring;

    public static void Luas(){
        System.out.println("Input alas");
        alas = input.nextInt();

        System.out.println("Input tinggi");
        tinggi  = input.nextInt();

        double luas = 0.5 * alas * tinggi;
        System.out.println("Luas = " + luas);
    }

    public static void Kelilinng(){
        System.out.println("Input alas");
        alas = input.nextInt();

        System.out.println("Input tinggi");
        tinggi  = input.nextInt();

        System.out.println("Input Sisi Miring");
        miring = input.nextInt();

        int keliling = alas + tinggi + miring;
        System.out.println("Keliling = " + keliling);
    }
}
