package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);
    private static double radius;
    private static double pi = 3.14;

    public static void Luas(){
        System.out.println("Input radius");
        radius = input.nextInt();

        double luas = pi * radius * radius;
        System.out.println("Luas = " + luas);
    }

    public static void Kelilinng(){
        System.out.println("Input radius");
        radius = input.nextInt();

        double keliling = 2 * pi * radius;
        System.out.println("Keliling = " + keliling);
    }
}
