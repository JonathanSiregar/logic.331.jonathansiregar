package BangunDatar;

import java.util.Scanner;

public class Persegi {

    private static Scanner input = new Scanner(System.in);
    private static int sisi;

    public static void Luas(){
        //  Luas Persegi Panjang
        System.out.println("Input Sisi (cm)");
        sisi = input.nextInt();

        int luas = sisi * sisi;
        System.out.println("Luas = " + luas);
    }

    public static void Keliling(){
        //Keliling
        System.out.println("Input sisi (cm)");
        sisi = input.nextInt();

        int keliling = 4 * sisi;
        System.out.println("Keliling = " + keliling);
    }

}
