package WarmupLMS;

import java.util.Scanner;

public class Soal06 {
    public static void Resolve(){// sama dengan no 7 array2d

        System.out.println("Masukkan panjang sisi staircase");
        Scanner input = new Scanner(System.in);

        int n = input.nextInt();
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                if(j<n-1-i){
                    System.out.print(" ");
                }else{
                    System.out.print("#");
                }
            }
            System.out.println("");
        }
    }
}
