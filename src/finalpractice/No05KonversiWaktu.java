package finalpractice;

import java.util.Scanner;

public class No05KonversiWaktu {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jam yg ingin dikonversi (dalam AM/PM)");
        System.out.println("Dengan Format HH:mm:ss AM/PM");
        System.out.println("03:04:44 PM = 15:04:44");
        String inputJam = input.nextLine();

        String output = "";

        String substringJam = inputJam.substring(0,2);//untuk ambil subtring jam

        String substringMenit  = inputJam.substring(3,5);// untuk ambil menit dari input

        String substringDetik = inputJam.substring(6,8);

        int konversiSubstringJam = Integer.parseInt(substringJam);

        int konversiSubstringMenit = Integer.parseInt(substringMenit);

        int konvesiSubstringDetik = Integer.parseInt(substringDetik);

        if (inputJam.contains("AM")){
            if(konversiSubstringJam == 12){
                output = "00" + ":" + konversiSubstringMenit + ":" + konvesiSubstringDetik;
            }
            else {
                output = inputJam.substring(0,7); //ambil jam JJ:MM
            }
        }
        else if (inputJam.contains("PM")) {
            if(konversiSubstringJam == 12){
                output = konversiSubstringJam + ":" + konversiSubstringMenit + ":" + konvesiSubstringDetik;
            }
            else{
                output = (konversiSubstringJam + 12) + ":" + konversiSubstringMenit + ":" + konvesiSubstringDetik;
            }
        }

        System.out.println("Jam Hasil Konversi = " + output);
    }
}
