package finalpractice;

import java.util.Scanner;

public class No07MedianMeanModusBelumSelesai {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("masukkan deret angka");
        String inputDeret = input.nextLine();
        
        int total = 0;
        int mean = 0;
        double median = 0;
        int modus = 0;
        
        String[] inputArray = inputDeret.split(" ");
        int[] intDeret = new int[inputArray.length];

        for (int i = 0; i < inputArray.length; i++) {
            intDeret[i] = Integer.parseInt(inputArray[i]);
        }

        for (int i = 0; i < intDeret.length; i++) {
            if(intDeret.length % 2 == 0){
                int i1 = intDeret[intDeret.length / 2];
                int i2 = intDeret[(intDeret.length / 2) + 1];
                double i1konversi = Double.valueOf(i1);
                double i2konversi = Double.valueOf(i2);
                median = (i1konversi + i2konversi) / 2.0;

                total = total + intDeret[i];
                mean = total / intDeret.length;
            }
            else {
                median = intDeret[(intDeret.length - 1) / 2] + intDeret[intDeret.length / 2] / 2;

                total = total + intDeret[i];
                mean = total / intDeret.length;
            }
            if(intDeret[i] > modus){
                modus = i;
            }
        }

        System.out.println("Mean = " + mean);
        System.out.println("Median = " + median);
        System.out.println("Modus = " + intDeret[modus]);
    }
}
