package finalpractice;

import java.util.Scanner;

public class No10Cupcake {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jumlah cupcake yang ingin dibuat");
        int inputCupcake = input.nextInt();

        double terigu = 125.0/15.0;
        double gula = 100.0/15.0;
        double susu = 100.0/15.0;

        double jumlahTerigu = (inputCupcake * terigu);
        double jumlahGula = (inputCupcake * gula);
        double jumlahSusu = (inputCupcake * susu);

        System.out.println("Jumlah yang takaran yang dibutuhkan untuk membuat " + inputCupcake + " Cupcake adalah " + jumlahTerigu + " gr Terigu, " + jumlahGula + " gr Gula, " + jumlahSusu + " mL Susu");

    }
}
