package finalpractice;

import java.util.Scanner;

public class No02Palindrome {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Kalimat palindrome");
        System.out.println("Kalimat yang bila dibaca dari depan dan belakang sama saja");
        System.out.println("Contoh : Katak");
        System.out.println("Masukkan string yang akan dicek");
        String inputKalimat = input.nextLine();

        char[] kalimatAsli = inputKalimat.toCharArray();

        char[] kalimatBalik = new char[kalimatAsli.length];

        int counter = 1;

        for (int i = 0; i < kalimatAsli.length; i++) {
            kalimatBalik[i] = kalimatAsli[kalimatAsli.length - counter];
            counter++;
        }

        int hitung = 1;

        for (int i = 0; i < kalimatBalik.length; i++) {
            if(kalimatBalik[i] == kalimatAsli[i]){
               hitung++;
               if (hitung == kalimatBalik.length){
                   System.out.println("Palindrome");
               }
            }
            else {
                System.out.println("Bukan Palindrome");
                break;
            }
        }
    }
}
