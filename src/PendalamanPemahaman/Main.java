package PendalamanPemahaman;

import ProbemSolving14Soal.Soal02;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;
        boolean flag  = true;
        String answer = "y";

        while (flag){

            System.out.println("Pilihan soal (1 - 10)");
            pilihan = input.nextInt();

            while(pilihan < 1 || pilihan > 12){
                System.out.println("Angka tida tersedia");
                pilihan = input.nextInt();
            }

            //System.out.println("Masukkan nilai n = ");
            //n = input.nextInt();


            switch (pilihan){
                case 1 :
                    //Soal01.Resolve();
                    break;
                case  2 :
                   // StrongPassword.Resolve(n);
                    break;
                case 3 :
                    Soal02.Resolve();
                    break;
                case 4 :
                    //MarsExploration.Resolve(args);
                    break;
                case 5 :
                    //HackerrankInAString.Resolve();
                    break;
                case 6 :
                    //Pangrams.Resolve();
                    break;
//                case 6 :
//                    Soal06.Resolve();
//                    break;
//                case 7 :
//                    Soal07.Resolve();
//                    break;
//                case 8 :
//                    Soal08.Resolve();
//                    break;
//                case 9 :
//                    Soal09Komplit66persen.Resolve();
//                    break;
//                case 10 :
//                    Soal10.Resolve();
//                    break;
//                case 11 :
//                    //Soal11.Resolve();
//                    break;
//                case 12 :
//                    //Soal12.Resolve
//                    break;
            }

            System.out.println("Try again? y/n");
            input.nextLine(); //skip bug
            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }
    }
}