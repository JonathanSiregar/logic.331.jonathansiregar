package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Rute Grosir X");
        System.out.println("Contoh input = 1-2-3-4");
        System.out.println("Masukkan rute");

        String ruteInput = input.nextLine();

        String[] ruteSplit = ruteInput.split("-");

        int[] rutePanjang = new int[ruteSplit.length];//array kosong [0 0 0 0] untuk menampung hasil konvert dari perulangan dibawah

        //Perulangan untuk mengubah value array ruteSplit sepanjang banyak data ruteLength dan mengisi array rutePanjang
        for (int i = 0; i < rutePanjang.length; i++) {
            rutePanjang[i] = Integer.parseInt(ruteSplit[i]);
        }

        //Array jarak toko X,1,2,3,4
        double[] toko = {0, 0.5, 2, 3.5, 5};

        //Mengambil nilai posisi pada array input
        int posisiToko = 0;

        //Menghitung jarak perpindahan antar toko
        double jarakPerpindahan = 0;

        //Mengambil nilai jarak toko pada posisi sebelumnya
        double posisiSebelumnya = 0;

        //Menghitung jarak total yg ditempuh
        double jarakTotal = 0;

        for (int i = 0; i < rutePanjang.length; i++) {
            posisiToko = rutePanjang[i];
            jarakPerpindahan = toko[posisiToko] - posisiSebelumnya;
            posisiSebelumnya = toko[posisiToko];
            jarakTotal += Math.abs(jarakPerpindahan);
        }

        double jarakBalik = toko[posisiToko] + jarakTotal;

        //Kecepatan per menit
        double kecepatan = 0.5;

        //Menghitung waktu yg dihabiskan di toko berdasarkan banyak input
        double delayToko = 10 * rutePanjang.length;

        //Menghitung waktu total
        double waktu = (jarakBalik / kecepatan) + delayToko;

        System.out.println("Waktu yang dihabiskan = " + waktu + " Menit");
    }
}
