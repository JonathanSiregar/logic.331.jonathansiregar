package ProbemSolving14Soal;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal12 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Inputan Buah dan Angka");
        String buahInput = input.nextLine();

        String[] dataArray = buahInput.split(",");

        String[][] dataBuah2Dimensi = new String[2][dataArray.length]; //membuat tampungan


        //mengisi array dataBuah2Dimensi
        for (int i = 0; i < dataArray.length; i++) {

            String[] split = dataArray[i].trim().split(":");

            for (int j = 0; j < split.length; j++) {

                dataBuah2Dimensi[j][i] = split[j];
            }
        }

        HashMap<String, Integer> buah = new HashMap<String, Integer>();

        for (int i = 0; i < dataBuah2Dimensi[0].length ; i++) {// cek

            String buahCek = dataBuah2Dimensi[0][i];
            System.out.println("Cek : " + buah.containsKey(buahCek));

            //buat ngecek data apakah ada di hashmap
            if(buah.containsKey(buahCek)){

                int dataSebelumnya = buah.get(buahCek); //untuk mengambul nilai value pada buah hashmap
                int dataAmbil = Integer.parseInt(dataBuah2Dimensi[1][i]);//buat ambil value dari input buah 2d
                int nilaiBaru = dataSebelumnya + dataAmbil;//buat hitung jumlah nama buah yang sama
                buah.replace(buahCek, dataSebelumnya, nilaiBaru);//replace buah dengan jumlah baru
            }
            else {

                int angka = Integer.parseInt(dataBuah2Dimensi[1][i]);
                buah.put(buahCek, angka);

            }
        }

        String[] sortString = new String[buah.size()];//ambil panjang hashmap
        int index = 0;

        for(String key : buah.keySet()){//keyset = untuk ambil nama
            sortString[index] = key + " : " + buah.get(key); //.get(key) untuk mengambil value nama buah dari hashmap buah
            index++ ;
        }

        Arrays.sort(sortString);

        for (int i = 0; i < sortString.length; i++) {
            System.out.println(sortString[i]);
        }
    }
}
