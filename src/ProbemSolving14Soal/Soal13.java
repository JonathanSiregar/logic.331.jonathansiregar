package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal13 {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();

        System.out.println("Masukkan alphabet");
        String inputText = input.nextLine();

        System.out.println("Masukkkan angka");
        String inputAngka = input.nextLine();

        //mengubah String Input menjadi array char
        char[] textInputArray = inputText.toLowerCase().toCharArray();

        //Mensplit input angka
        String[] inputAngkaSplit = inputAngka.split(",");

        //Membuat array tampungan
        String[] output = new String[inputAngkaSplit.length];

        for (int i = 0; i < inputAngkaSplit.length; i++) {
            int inputAngkaKonversi = Integer.parseInt(inputAngkaSplit[i]); //konversi ke int
            char textInput = textInputArray[i]; //

            if(alphabet[inputAngkaKonversi - 1] == textInput){
                output[i] = "True";

            }
            else{
                output[i] = "False";
            }
        }

        for (int i = 0; i < output.length; i++) {
            System.out.println("Output  = " + output[i]);
        }

    }
}
