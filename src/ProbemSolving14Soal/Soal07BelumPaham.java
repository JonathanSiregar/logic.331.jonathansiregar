package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal07BelumPaham {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Rute");
        System.out.println("Contoh : ---o-o");
        String polaLintasan = input.nextLine();

        System.out.println("Masukkan cara berjalan");
        System.out.println("Contoh : wwwwjj");
        System.out.println("w = walk; geser 1, dapat 1 energi");
        System.out.println("j = jump, konsumsi 2 energi");
        String inputCaraJalan = input.nextLine();

        char[] polaChar = polaLintasan.toCharArray();

        char[] caraJalanChar = inputCaraJalan.toCharArray();

        int lompat = 0;

        int energi = 0;

        String output = "";

        for (int i = 0; i < caraJalanChar.length; i++) {
            char ruteJalan = polaChar[i + lompat]; //ambil data rute jalan dari inputan,
            char jalan = caraJalanChar[i];

            if(jalan == 'w' && ruteJalan == '-'){
                energi++;
                output = String.valueOf(energi);
            }
            else if (jalan == 'w' && ruteJalan == 'o') {
                output = "Jim Mati";
                break;
            }
            else if (jalan == 'j' && (ruteJalan == '-' || ruteJalan == 'o') && energi >= 2) {
                lompat += 1;
                energi -= 2;
                output = String.valueOf(energi);
            }
            else if (jalan == 'j' && ruteJalan == 'o' && energi < 2) {
                output = "Jim Mati";
                break;
            }
        }

        System.out.println("Output = " + output);

    }
}
