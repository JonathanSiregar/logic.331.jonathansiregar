package ProbemSolving14Soal;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal03 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Inputan Buah dan Angka");
        String buahInput = input.nextLine();

        String[] dataArray = buahInput.split(",");

        String[][] dataBuah2Dimensi = new String[2][dataArray.length]; //membuat tampungan


        //mengisi array dataBuah2Dimensi
        for (int i = 0; i < dataArray.length; i++) {

            String[] split = dataArray[i].trim().split(":");

            for (int j = 0; j < split.length; j++) {

                dataBuah2Dimensi[j][i] = split[j]; // mengisi baris terlebih dahulu baru kolom
            }
        }

        HashMap<String, Integer> buah = new HashMap<String, Integer>(); //agar dapat memudahkan modifikasi nama yang sama, key = String, value = Integer

        for (int i = 0; i < dataBuah2Dimensi[0].length ; i++) {//mengolah data memasukkan ke hashmap

            String buahCek = dataBuah2Dimensi[0][i];
            System.out.println("Cek : " + buah.containsKey(buahCek));

            //buat ngecek data apakah ada di hashmap
            if(buah.containsKey(buahCek)){

                int dataSebelumnya = buah.get(buahCek); //untuk mengambul nilai value pada buah hashmap
                int dataAmbil = Integer.parseInt(dataBuah2Dimensi[1][i]);//buat ambil jumlah dari input array buah 2d
                int nilaiBaru = dataSebelumnya + dataAmbil;//buat hitung jumlah nama buah yang sama
                buah.replace(buahCek, dataSebelumnya, nilaiBaru);//replace buah dengan jumlah baru
            }
            else {

                int angka = Integer.parseInt(dataBuah2Dimensi[1][i]);
                buah.put(buahCek, angka); //masukkan input bila tidak ada yang sama

            }
        }

        String[] sortString = new String[buah.size()];//membuat tampungan untuk dimasukkan ke array dari hashmap

        int index = 0; //untuk membrikan posisi pada array sortstring

        for(String key : buah.keySet()){//keyset = untuk ambil nama // for each =
            sortString[index] = key + " : " + buah.get(key); //.get(key) untuk mengambil value nama buah dari hashmap buah
            index++ ;
        }

        Arrays.sort(sortString); // untuk mengurutkan secara ascending

        for (int i = 0; i < sortString.length; i++) {
            System.out.println(sortString[i]);
        }
    }
}


//coba kalo input kata sama dengan beda huruf besar dan huruf kecil