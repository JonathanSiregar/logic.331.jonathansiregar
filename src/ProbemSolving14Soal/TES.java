package ProbemSolving14Soal;

public class TES {
    public static void main(String[] args) {
        String input = "1-2-3-4"; // Input jarak antar toko
        double lajuMotorKmPerJam = 30.0; // Laju rata-rata motor (km/jam)
        int waktuPerToko = 10; // Waktu rata-rata di setiap toko (menit)

        // Jarak dari grosir ke masing-masing toko (dalam kilometer)
        double[] jarakToko = {0.5, 2.0, 3.5, 5.0};

        // Membagi input menjadi array toko
        String[] tokoArray = input.split("-");
        int totalToko = tokoArray.length;

        // Menghitung total jarak yang harus ditempuh
        double totalJarak = 0.0;
        for (String toko : tokoArray) {
            int tokoIndex = Integer.parseInt(toko) - 1;
            totalJarak += jarakToko[tokoIndex];
        }

        // Menghitung waktu yang dibutuhkan untuk menempuh total jarak
        double waktuPerjalananJam = totalJarak / lajuMotorKmPerJam;

        // Menghitung total waktu, termasuk waktu di toko
        double totalWaktuMenit = waktuPerjalananJam * 60 + totalToko * waktuPerToko;

        System.out.println("Output: " + (int) totalWaktuMenit + " menit");
}

}
