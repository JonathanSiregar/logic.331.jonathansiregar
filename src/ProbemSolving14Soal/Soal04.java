package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan input jumlah uang : ");
        int uang = input.nextInt();

        System.out.println("Masukkan jumlah barang : ");
        int jumlahBarang = input.nextInt();

        input.nextLine(); // skip bug

        System.out.println("Masukkan nama barang");

        String[][] namaBarang2Dimensi = new String[2][jumlahBarang]; // tampungan

        //mengisi array namaBarang2Dimensi
        for (int i = 0; i < jumlahBarang; i++) {
            String namaBarang  = input.nextLine();
            String[] namaBarangSplit = namaBarang.split(",");

            for (int j = 0; j < namaBarangSplit.length; j++) {
                namaBarang2Dimensi[j][i] = namaBarangSplit[j];
            }
        }

        String hasil = ""; // nampung barang yg bisa dibeli

        for (int i = 0; i < jumlahBarang; i++) { //
            String namaBarang = namaBarang2Dimensi[0][i];
            int hargaBarang = Integer.parseInt(namaBarang2Dimensi[1][i]); // mengubah baris index 1 yang berisi String harga ke int

            if(uang >= hargaBarang){
                uang -= hargaBarang;
                hasil += namaBarang + " , ";
            }
        }

        System.out.println(hasil);

    }
}
