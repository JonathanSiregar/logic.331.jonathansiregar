package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Jumlah Pulsa");
        int inputPulsa = input.nextInt();

        int poin1 = 0;
        int poin2 = 0;
        int poin3 = 0;

        if(inputPulsa <= 10000){
             poin1 = 0;
        }
        if(inputPulsa > 10000){
            if (inputPulsa > 30000){
                poin2 = 20;
            }
            else {
                poin2 = (inputPulsa - 10000) / 1000;
            }
        }
        if (inputPulsa > 30000){
            poin3 = ((inputPulsa - 30000) / 1000) * 2;
        }

        int output = poin1 + poin2 + poin3;
        System.out.println("Total poin anda adalah = " + poin1 + " + " + poin2 + " + " + poin3 + " = " + output);

    }
}
