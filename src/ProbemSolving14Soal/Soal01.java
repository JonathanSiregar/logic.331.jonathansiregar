package ProbemSolving14Soal;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan panjang deret");
        int n = input.nextInt();

        int helper = 1;

        int[][] hasil = new int[2][n];

        for (int i = 0; i < 2; i++) {

            System.out.print("Deret ke " + (i+1));
            System.out.println("");

            for (int j = 0; j < n; j++) {

                if(i == 0){
                    hasil[i][j] = (j * 3) - 1; //buat array pertama
                    System.out.print(hasil[0][j]+ " ");
                }
                else{
                    hasil[i][j] = (j * -2) * 1; // buat array kedua
                    System.out.print(hasil[1][j] + " ");
                }
            }
            System.out.println("");
        }


        System.out.println("Hasilnya = ");
        for (int i = 0; i < n; i++) {

            System.out.print((hasil[0][i] + hasil[1][i]) + " ");
        }
        System.out.println("");
    }
}
