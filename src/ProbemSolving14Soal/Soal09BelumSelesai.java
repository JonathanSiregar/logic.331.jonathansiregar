package ProbemSolving14Soal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Soal09BelumSelesai {

    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        DateFormat df = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        System.out.println("+=====+ Sistem Parkir +====+");
        System.out.println("1. 8 jam pertama : 1000 per jam");
        System.out.println("2. lebih dari 8 jam - 24 jam : 8000 flat");
        System.out.println("3. lebih dari 24 jam : 1500 / 24 jam");
        System.out.println("Contoh Input : 28 january 2020 07:30:34");
        System.out.println("Note : pastikan tidak ada typo, spasi sesuai dan bulan menggunakan bahasa inggris");

        // Olah Input
        System.out.println("Input tanggal masuk :");
        String masukInput = input.nextLine();
        boolean flag1 = true;
        Date masukDate = null;
        while (flag1){
            try{
                masukDate = df.parse(masukInput);
                flag1 = false;
            }catch (Exception e){
                System.out.println("Format input tidak sesuai");
                System.out.println("Contoh Input : 28 january 2020 07:30:34");
                System.out.println("Input tanggal masuk :");
                masukInput = input.nextLine();
            }
        }
        System.out.println("Input tanggal keluar :");
        String keluarInput = input.nextLine();
        Date keluarDate = null;
        boolean flag2 = true;
        while (flag2){
            try{
                keluarDate = df.parse(keluarInput);
                flag2 = false;
            }catch (Exception e){
                System.out.println("Format input tidak sesuai");
                System.out.println("Contoh Input : 28 january 2020 07:30:34");
                System.out.println("Input tanggal masuk :");
                keluarInput = input.nextLine();
            }
        }
//        System.out.println(masukDate);
//        System.out.println(keluarDate);

        // OLAH DATA (karena nilai besar pakai long)
        long selisihWaktu = Math.abs(masukDate.getTime() - keluarDate.getTime());
        // get.date() => representasi tgl 1 januari 1970 dalam milisecond
        // untuk merubahnya jadi perlu merubah milisecond > second > menit > jam
        // 1000 / 60 / 60
//        System.out.println("masuk = "+ masukDate.getTime());
//        System.out.println("keluar = "+ keluarDate.getTime());
//        System.out.println(selisihWaktu);

        double selisihJam = (double) ((selisihWaktu / 1000) / 60) / 60;
        System.out.println("selisih = " + selisihJam + " jam");
        int output = 0;
        boolean flagHitung = true;

        while (flagHitung){
            if (selisihJam < 8){
                output += selisihJam * 1000;
                flagHitung = false;
            } else if (selisihJam > 8 && selisihJam < 24) {
                output += 8000;
                flagHitung = false;
            }else {
                output += 15000;
                selisihJam -= 24;
            }
        }
        System.out.println("Output = " + output);
    }
}