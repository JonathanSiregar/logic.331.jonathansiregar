package ProbemSolving14Soal;

public class Utility {
    public static int[] ConvertStringToArrayInt(String jumlahKacamata){

        String[] textArray = jumlahKacamata.split(",");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);

        }
        return intArray;
    }
}
