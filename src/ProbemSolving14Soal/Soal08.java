package ProbemSolving14Soal;

import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan input jumlah uang : ");
        int uang = input.nextInt();

        input.nextLine(); // skip bug

        System.out.println("Masukkan Harga Kacamata : ");
        String inputKacamata = input.nextLine();

        System.out.println("Masukkan Harga Baju");
        String inputBaju = input.nextLine();

        //Split input Kacamata
        String[] listKacamataSplit = inputKacamata.split(",");

        //Membuat array list untuk tampungan kacamata
        int[] intKacamata = new int[listKacamataSplit.length];

        //Konvert ke int
        for (int i = 0; i < intKacamata.length; i++) {
            intKacamata[i] = Integer.parseInt(listKacamataSplit[i]);
        }

        //Split input Baju
        String[] listBajuSplit = inputBaju.split(",");

        //Membuat array list untuk tampungan baju
        int[] intBaju = new int[listBajuSplit.length];

        //Konvert ke int
        for (int i = 0; i < intBaju.length; i++) {
            intBaju[i] = Integer.parseInt(listBajuSplit[i]);
        }

        //membuat array tampungan hasil penjumlahan kacamata dan baju
        int[] totalKacamataBaju = new int[intKacamata.length * intBaju.length];
        int counter = 0;

        //menambahkan value intKacamata dan intBaju
        for (int i = 0; i < intKacamata.length; i++) {
            for (int j = 0; j < intBaju.length; j++) {
                totalKacamataBaju[counter] = intKacamata[i] + intBaju[j];
                counter++;
            }
        }

        //Mengurutkan value array dari kecil ke besar
        Arrays.sort(totalKacamataBaju);

        //Mendeklarasikan nilai output
        int output = 0;

        for (int i = 0; i < totalKacamataBaju.length; i++) {
            if(totalKacamataBaju[i] <= uang){
                output = totalKacamataBaju[i];
            }
        }
        System.out.println("Total Harga = " + output);
    }
}
