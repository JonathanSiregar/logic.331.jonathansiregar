package ProbemSolving14Soal;

import javax.xml.crypto.Data;
import java.util.Scanner;

public class Soal10 {
    private static String[] DataWadah = {"teko", "botol", "gelas", "cangkir"};

    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan pilihan yang ingin dikonversi");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        int jenisWadah = input.nextInt();

        jenisWadah -= 1; //untuk memanggil index array DataWadah

        System.out.println("Masukkan jumlah wadah yg ingin konversi");
        int inputJumlah = input.nextInt();

        System.out.println("Ingin dikonversi ke ?");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        int inputKonversi = input.nextInt();


        switch (inputKonversi){
            case 1 :
                konversiTeko(DataWadah[jenisWadah],inputJumlah);
                break;
            case 2 :
                konversiBotol(DataWadah[jenisWadah],inputJumlah);
                break;
            case 3 :
                konversiGelas(DataWadah[jenisWadah],inputJumlah);
                break;
            case 4 :
                konversiCangkir(DataWadah[jenisWadah],inputJumlah);
                break;
        }
    }
    //"teko", "botol", "gelas", "cangkir"
    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   ==>  10   ==>   25
    private static void konversiTeko(String pilihanWadah, int jumlah){
        double hasilKonversi = 0;
        String namaKonversi = DataWadah[0];

        if(pilihanWadah.equals(DataWadah[1])){
            hasilKonversi = jumlah / 5.0;
        }
        else if (pilihanWadah.equals(DataWadah[2])) {
            hasilKonversi = jumlah / 10.0;
        }
        else if (pilihanWadah.equals(DataWadah[3])){
            hasilKonversi = jumlah / 25.0;
        }
        else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihanWadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void konversiBotol(String pilihanWadah, int jumlah){
        double hasilKonversi = 0;
        String namaKonversi = DataWadah[1];

        if(pilihanWadah.equals(DataWadah[0])){
            hasilKonversi = jumlah * 5.0;
        }
        else if (pilihanWadah.equals(DataWadah[2])) {
            hasilKonversi = jumlah / 2.0;
        }
        else if (pilihanWadah.equals(DataWadah[3])){
            hasilKonversi = jumlah /5.0;
        }
        else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihanWadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void konversiGelas(String pilihanWadah, int jumlah){
        double hasilKonversi = 0;
        String namaKonversi = DataWadah[2];

        if(pilihanWadah.equals(DataWadah[0])){
            hasilKonversi = jumlah * 10.0;
        }
        else if (pilihanWadah.equals(DataWadah[1])) {
            hasilKonversi = jumlah * 2.0;
        }
        else if (pilihanWadah.equals(DataWadah[3])){
            hasilKonversi = jumlah / 10.0;
        }
        else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihanWadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    private static void konversiCangkir(String pilihanWadah, int jumlah){
        double hasilKonversi = 0;
        String namaKonversi = DataWadah[3];

        if(pilihanWadah.equals(DataWadah[0])){
            hasilKonversi = jumlah * 25.0;
        }
        else if (pilihanWadah.equals(DataWadah[1])) {
            hasilKonversi = jumlah * 5.0;
        }
        else if (pilihanWadah.equals(DataWadah[2])){
            hasilKonversi = jumlah * 2.5;
        }
        else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihanWadah + " = " + hasilKonversi + " " + namaKonversi);
    }

}
