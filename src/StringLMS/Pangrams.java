package StringLMS;

import java.util.Scanner;

public class Pangrams {
    public static void Resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan String");
        String text = input.nextLine();

        String lowercase = "abcdefghijklmnopqrstuvwxyz";



        int counter = 0;

        String kalimat = text.trim().toLowerCase();


        for(int i = 0 ; i < lowercase.length() ; i ++){
            for (int j = 0; j < kalimat.length(); j++) {
                if(lowercase.charAt(i) == kalimat.charAt(j)){
                    counter++ ;
                    break;
                }
            }
        }
        if(counter == 26){
            System.out.println("Kalimat adalah pangrams");
        }
        else {
            System.out.println("Kalimat bukan pangrams");
        }
    }
}
