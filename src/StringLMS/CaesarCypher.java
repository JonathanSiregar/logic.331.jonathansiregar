package StringLMS;

import java.util.Scanner;

public class CaesarCypher {
    public static void Resolve(int n) {
       Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kalimat yang ingin di enkripsi :");
        String message = input.nextLine();
        int key = 2; // Jumlah pergeseran (3 karakter ke kanan)

        String encryptedMessage = encrypt(message, key);
        System.out.println("Pesan terenkripsi: " + encryptedMessage);

        String decryptedMessage = decrypt(encryptedMessage, key);
        System.out.println("Pesan terdekripsi: " + decryptedMessage);
    }

    public static String encrypt(String message, int key) {
        StringBuilder encryptedText = new StringBuilder();

        for (char character : message.toCharArray()) {
            if (Character.isLetter(character)) {
                char base = Character.isUpperCase(character) ? 'A' : 'a';
                char encryptedChar = (char) ((character - base + key) % 26 + base);
                encryptedText.append(encryptedChar);
            } else {
                encryptedText.append(character);
            }
        }

        return encryptedText.toString();
    }

    public static String decrypt(String encryptedMessage, int key) {
        return encrypt(encryptedMessage, 26 - key); // Decrypting is just shifting in the opposite direction
    }

}
