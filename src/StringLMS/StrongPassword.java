package StringLMS;

import java.util.Scanner;

public class StrongPassword {
    private static boolean cekKriteria(String password, String criteria) {
        for (char c : password.toCharArray()) {
            if (criteria.indexOf(c) != -1) {
                return true;
            }
        }
        return false;
    }
    public static void Resolve(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Input :");
        int s = input.nextInt();
        String password =input.next();
        input.close();

        String numbers = "0123456789";
        String lower_case = "abcdefghijklmnopqrstuvwxyz";
        String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String special_characters = "!@#$%^&*()-+";

        int Kriteria = 0 ;
        // ngecek kriteria masuk kemana
        if(!cekKriteria(password,numbers)){
            Kriteria = 0;
        }
        if(!cekKriteria(password,lower_case)){
            Kriteria = 0;
        }
        if(!cekKriteria(password,upper_case)){
            Kriteria = 0;
        }
        if(!cekKriteria(password,special_characters)){
            Kriteria = 0;
        }
        if (s + Kriteria < 6) {
            Kriteria = 6 - (s + Kriteria);
        }
        System.out.println(Kriteria);
    }

}
