package StringLMS;

import WarmupLMS.Utility;

import java.util.Scanner;

public class TwoStringsBelumSelesai {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input Deret Angka yg pertama");
        String input1 = input.nextLine();

        System.out.println("Input Deret Angka yg kedua");
        String input2 = input.nextLine();

        int poinDeret1 = 0, poinDeret2 = 0;

        int[] deret1 = WarmupLMS.Utility.ConvertStringToArrayInt(input1);
        int length1 = deret1.length;;

        int[] deret2 = Utility.ConvertStringToArrayInt(input2);
        int length2 = deret2.length;

        for (int i = 0; i < length1; i++) {
            if (deret1[i] > deret2[i]){
                poinDeret1++;
            }
            else if (deret2[i] > deret1[i]) {
                poinDeret2++;
            }

        }

        System.out.println("Poin perbandingan akhir adalah" + "Poin deret 1 = " + poinDeret1 + " "+ "Poin deret 2 = " + poinDeret2);

    }
}
