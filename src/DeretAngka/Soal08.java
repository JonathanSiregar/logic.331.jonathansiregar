package DeretAngka;

public class Soal08 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int deret = 3;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = deret;
            deret *= 3;
        }
        Utility.PrintArray1D(hasil);
    }
}
