package DeretAngka;

public class Soal07 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int deret = 2;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = deret;
            deret *= 2;
        }
        Utility.PrintArray1D(hasil);
    }
}
