package DeretAngka;

public class Soal04 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int deret = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = deret;
            deret += 4;
        }
        Utility.PrintArray1D(hasil);
    }
}
