package DeretAngka;

public class Soal01 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int ganjil = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = ganjil;
            ganjil += 2;
        }
        Utility.PrintArray1D(hasil);
    }
}
