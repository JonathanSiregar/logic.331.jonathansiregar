package DeretAngka;

public class Soal02 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int genap = 2;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            hasil[i] = genap;
            genap += 2;
        }
        Utility.PrintArray1D(hasil);
    }
}
