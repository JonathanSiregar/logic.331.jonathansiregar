package DeretAngka;

public class Soal09 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int deret = 4;
        int hitung = 1;

        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            if(hitung % 3 == 0){
                hasil[i] = 0;
                hitung++;
            }else {
                hasil[i] = deret;
                deret *=4;
                hitung++;
            }

        }
        Utility.PrintArray1D(hasil);
    }
}
