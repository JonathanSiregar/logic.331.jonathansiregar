package DeretAngka;

public class Soal10 {
    public static void Resolve (int n){//untuk parameter pengirman data ke class main

        int awal = 3;
        int tanda = 1;
        int[] hasil = new int[n];

        for (int i = 0; i < n; i++) {
            if (tanda % 4 == 0){
                hasil[i] = 111;
                awal *= 3;
                tanda += 1;
            }else {
                hasil[i] = awal;
                awal *= 3;
                tanda += 1;
            }
        }
        Utility.PrintXXX(hasil);
    }
}
