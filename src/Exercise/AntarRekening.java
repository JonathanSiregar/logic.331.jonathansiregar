package Exercise;

import java.util.Scanner;

public class AntarRekening {

    private static Scanner input = new Scanner(System.in);
    private static boolean flag1 = true;
    private static boolean flag2 = true;

    public static void TranferRekening(){
        int saldo = EmpatSatu.getSaldo();

        while(flag1){
            System.out.println("Masukkan nomor rekeing tujuan :");
            String nomorRekening = input.next();
            Validasi.ValidasiPin(nomorRekening);
            if(nomorRekening.length() !=7 ){
                System.out.println("Nomor rekening salah");
                System.out.println("Silahkan masukkan kembali");
            }
            else{
                while(flag2){
                    System.out.println("Silahkan masukkan nominal tranfer");
                    String uangTransfer = input.next();
                    if(Validasi.ValidasiPin(uangTransfer)){
                        int uangTransferKonversi = Integer.parseInt(uangTransfer);
                        if(uangTransferKonversi > saldo){
                            System.out.println("Saldo tidak mencukupi");
                            System.out.println("silahkan masukkan nominal kembali");
                        }
                        else{
                            System.out.println("Transaksi berhasil");
                            saldo -= uangTransferKonversi; //ekivalen dengan saldo = saldo - uangTransfer
                            EmpatSatu.setSaldo(saldo);
                            System.out.println("Saldo Anda sekarang adalah = Rp " + saldo);
                            System.out.println("Apakah Anda ingin melakukan transaksi lagi ?  y/n");
                            input.nextLine();
                            String konfirmasi = input.nextLine();
                            if(!konfirmasi.toLowerCase().equals("y")){
                                flag2 = false; //kenapa flag2 duluan yg di false
                                flag1 = false;
                                break;
                            }
                            else {
                                flag2 = false;
                                flag1 = false;
                                Dua.MasukPin();
                            }
                        }
                    }
                }
            }
        }
    }
}
