package Exercise;

import java.util.Scanner;

public class EmpatDua {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;

    public static void Transfer(){

        while(flag){
            System.out.println("Silahkan pilih jenis transfer");
            System.out.println("1. Antar Rekening");
            System.out.println("2. Antar Bank");
            System.out.println("Pastikan saldo mencukupi sebelum transfer");

            int pilihanTransfer = input.nextInt();

            if(pilihanTransfer == 1){
                AntarRekening.TranferRekening();
                flag = false;
            }
            else {
                AntarBank.TransferBank();
                flag = false;
            }
        }
    }
}
