package Exercise;

import java.util.Scanner;

public class EmpatSatu {

    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    private static int saldo;

    public static void SetoranTunai(){
        System.out.println("Silahkan Masukkan Nominal Uang");
        System.out.println("Maksimal transaksi adalah Rp 25.000.000");

        flag = true; // tanya kenapa harus ada disini, karena diatas kan sudah ada apa perlu ditambahkan agar bisa lanjut

        while(flag){
            System.out.println("Nominal uang = ");
            String uangMasuk = input.next();
            if (Validasi.ValidasiPin(uangMasuk)){
                int uangMasukkonversi = Integer.parseInt(uangMasuk);
                if(uangMasukkonversi > 25000000) {
                    System.out.println("Melebihi limit transaksi harian");
                    System.out.println("Silahkan masukkan nominal lagi");
                }
                else{
                    System.out.println("Are you sure ?    y/n");
                    input.nextLine(); // skip bugged digunakan bila sebelumnya ada input integer dan setelahnya string
                    String konfirmasiSetoran = input.nextLine();
                    if (!konfirmasiSetoran.toLowerCase().equals("y")){
                        System.out.println("Silahkan masukkan nominal setoran kembali");
                    }
                    else{
                        System.out.println("Setoran Tunai Anda telah berhasil");
                        saldo = uangMasukkonversi;
                        System.out.println("Saldo anda saat ini adalah = Rp " + uangMasukkonversi);
                        System.out.println("Apakah anda ingin melakukan transaksi lagi ? y/n");
                        String konfirmasilanjut = input.nextLine();
                        if(!konfirmasilanjut.toLowerCase().equals("y")){
                            flag = false;
                            break; //exit program
                        }
                        else {
                            flag = false;
                            Tiga.MenuTransaksi();
                        }
                    }
                }
            }
        }
    }
    public static int getSaldo(){
        return saldo;
    }
    public static void setSaldo(int saldo){// tanya kenapa ada int saldo di dalam tanda kurung
        EmpatSatu.saldo = saldo;
        // tanya kegunaan buat apa ???
        // hipotesis untuk set saldo di class transfer
    }

}
