package Exercise;

public class Validasi {
    public static boolean ValidasiPin (String numberString){
        if (numberString == null){
            return false;//tanya kegunaan ini
        }
        // TRY and CATCH digunakan untuk mendeteksi adanya error dari inputan dengan cara ????
        try{
            //Digunakan untuk mengubah tipe data string menjadi integer
            int validasi = Integer.parseInt(numberString);
        }
        catch (NumberFormatException exception){
            System.out.println("Pastikan PIN Hanya Angka");
            return false;
        }

        return true;
    }
}
