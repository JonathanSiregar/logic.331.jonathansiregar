package Exercise;

import java.util.Scanner;

public class AntarBank {
    private static Scanner input = new Scanner(System.in);

    private static boolean flag1 = true;
    private static boolean flag2 = true;

    public static void TransferBank(){
        int saldo = EmpatSatu.getSaldo();

        while(flag1){
            System.out.println("Masukkan nomor rekening tujuan ");
            System.out.println("Nomor rekening tujuan terdiri dari 3 angka kode bank tujuan dan 7 angka rekening");
            System.out.println("List Bank yang dapat ditransfer"); // masih bingung gimana cara bisa konfirmasi kode bank
            System.out.println("BRM = 011");
            System.out.println("BKK = 012");
            String nomorRekening = input.next();
            if(Validasi.ValidasiPin(nomorRekening)){
                if(nomorRekening.length() !=10 ){
                    System.out.println("Nomor rekening salah");
                    System.out.println("Silahkan masukkan kembali");
                }
                else{
                    while(flag2){
                        System.out.println("Silahkan masukkan nominal tranfer");
                        int uangTransfer = input.nextInt();
                        if(uangTransfer > saldo){
                            System.out.println("Saldo tidak mencukupi");
                            System.out.println("silahkan masukkan nominal kembali");
                        }
                        else{
                            System.out.println("Transaksi berhasil");
                            saldo -= uangTransfer; //ekivalen dengan saldo = saldo - uangTransfer
                            EmpatSatu.setSaldo(saldo);
                            System.out.println("Saldo Anda sekarang adalah = Rp " + saldo);
                            System.out.println("Apakah Anda ingin melakukan transaksi lagi ?  y/n");
                            input.nextLine();
                            String konfirmasi = input.nextLine();
                            if(!konfirmasi.toLowerCase().equals("y")){
                                flag2 = false; //kenapa flag2 duluan yg di false
                                flag1 = false;
                                break;
                            }
                            else {
                                flag2 = false;
                                flag1 = false;
                                Dua.MasukPin();
                            }
                        }
                    }
                }
            }
        }
    }
}
