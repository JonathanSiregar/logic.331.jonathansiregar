package Exercise;

import java.util.Scanner;

public class Dua {
    private static Scanner input = new Scanner(System.in);
    private static boolean flag = true;
    public static void MasukPin(){

        String buatPin = Satu.getSatu();

        int salahMasuk = 0;

        flag = true;

        while(flag){
            if (salahMasuk == 3){//Menghitung berapa kali salah masuk pin, saat pertama kali salah terhitung 1

                System.out.println("Maaf Rekening Anda Terblokir");

                flag=false;

                break;//digunakan untuk menghentikan percobaan masuk PIN
            }

            System.out.println("Silahkan Masukkan PIN");

            String masukPin = input.next();

            if (masukPin.equals(buatPin)){//Membandingkan string menggunakan equals

                flag = false;
                Tiga.MenuTransaksi();
            }
            if (!masukPin.equals(buatPin)){
                System.out.println("PIN Salah");
                System.out.println("Silahkan masukkan PIN kembali");
                System.out.println("Sisa Percobaan = " + (2 - salahMasuk));
                salahMasuk++;
            }
        }

    }

}
