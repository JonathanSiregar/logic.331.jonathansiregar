package Pretest;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan rute");
        System.out.println("Tempat 1 = 1");
        System.out.println("Tempat 2 = 2");
        System.out.println("Tempat 3 = 3");
        System.out.println("Tempat 4 = 4");
        System.out.println("Contoh = 1-2-3-4");
        String inputRute = input.nextLine();

        String[] ruteSplit = inputRute.split("-");

        int[] ruteTotal = new int[ruteSplit.length];

        for (int i = 0; i < ruteSplit.length; i++) {
            ruteTotal[i] = Integer.parseInt(ruteSplit[i]);
        }

        double[] jarakToko = {0,2,0.5,1.5,2.5};

        int posisiAwal = 0;

        double jarakPindah = 0;

        double posisiSebelumnya = 0;

        double jarakTotal = 0;

        for (int i = 0; i < ruteTotal.length; i++) {
            posisiAwal = ruteTotal[i];
            jarakPindah = jarakToko[posisiAwal] + posisiSebelumnya;
            posisiSebelumnya = jarakToko[posisiAwal];
            jarakTotal += Math.abs(jarakPindah);
        }

        double jarakPulang = jarakToko[posisiAwal] + jarakTotal;

        double bensin = 0.4;

        double bensinTotal = (jarakPulang * bensin);

        System.out.println("Bensin yang dihabiskan = " + bensinTotal + "liter");


    }
}
