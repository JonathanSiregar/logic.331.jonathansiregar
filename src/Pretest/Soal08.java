package Pretest;

import java.util.Scanner;

public class Soal08 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan panjang deret angka");
        int inputAngka = input.nextInt();

        int hitungBagi = 0;
        int isiArray = 0;

        int[] deretPrimer = new int[inputAngka];

        int cekPanjangPrimer = 5; //untuk mengecek panjang nilai bilangan primer degan panjang nilai input

        for (int i = 2; i < cekPanjangPrimer; i++) {//sebagai bilangan yang akan dicek
            for (int j = 1; j <= i; j++) {// untuk membagi nilai i dengan 1 dan nilai itu sendiri
                if(i % j == 0){
                    hitungBagi++;
                }
            }
            if (hitungBagi == 2){
                deretPrimer[isiArray] = i;
                isiArray++ ;

            }
            if (isiArray == inputAngka){ //untuk mengeck banyaknya bilangan primer sama dengan input
                break;
            }
            else {
                cekPanjangPrimer++; //untuk me
            }
            hitungBagi = 0; //reset ke 0, untuk

        }
        for (int i = 0; i < deretPrimer.length; i++) {
            System.out.print(deretPrimer[i] + " ");
        }

        System.out.println("");

        int[] deretFibonacci = new int[inputAngka];
        int awal = 1;
        int helper = 1;
        int sebelumnya = 0;

        for (int i = 0; i < inputAngka; i++) {
            deretFibonacci[i] = helper;
            helper = awal + sebelumnya;
            sebelumnya = awal;
            awal = helper;
            System.out.print(deretFibonacci[i] + " ");
        }

        System.out.println("");

        int[] totalDeret = new int[inputAngka];

        for (int i = 0; i < inputAngka; i++) {
            totalDeret[i] = deretPrimer[i] + deretFibonacci[i];
            System.out.print(totalDeret[i] + " ");
        }

        System.out.println("");

    }
}
