package Pretest;

import java.util.Scanner;

public class Soal05 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan jumlah laki - laki dewasa");
        int inputLakiDewasa = input.nextInt();

        System.out.println("Masukkan jumlah perempuan dewasa");
        int inputPerempuanDewasa = input.nextInt();

        System.out.println("Masukkan jumlah remaja");
        int inputRemaja = input.nextInt();

        System.out.println("Masukkan jumlah anak - anak");
        int inputAnak = input.nextInt();

        System.out.println("Masukkan jumlah balita");
        int inputBalita = input.nextInt();

        int porsiLaki = 2;
        int porsiWanita = 1;
        int porsiRemaja = 1;
        double porsiAnak = 0.5;
        int porsiBalita = 1;
        int totalTamu = inputLakiDewasa + inputPerempuanDewasa + inputRemaja + inputAnak + inputBalita;
        double totalPorsi = 0;

        if(totalTamu % 2 != 0 && totalTamu > 5){
            int porsiWanitaTambah = 2;
            totalPorsi = (inputLakiDewasa * porsiLaki) + (inputPerempuanDewasa * porsiWanitaTambah) + (inputRemaja * porsiRemaja) + (inputAnak * porsiAnak) + (inputBalita * porsiBalita);
        }
        else {
            totalPorsi = (inputLakiDewasa * porsiLaki) + (inputPerempuanDewasa * porsiWanita) + (inputRemaja * porsiRemaja) + (inputAnak * porsiAnak) + (inputBalita * porsiBalita);
        }

        System.out.println(totalPorsi);
    }
}
