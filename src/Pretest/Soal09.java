package Pretest;

import java.util.Scanner;

public class Soal09 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan rute Hatorri");
        System.out.println("Contoh : N N T T");
        String inputRute = input.nextLine();

        String inputRuteSplit = inputRute.trim().toLowerCase();

        char[] inputRuteArray = inputRuteSplit.toCharArray();

        int ketinggian = 0;
        int tambah = 0;
        int kurang = 0;

        int countGunung = 0;
        int countLembah = 0;

        for (int i = 0; i < inputRuteArray.length; i++) {
            if(inputRuteArray[i] == 'n'){
                tambah++;
            }
            else if(inputRuteArray[i] == 't'){
                kurang++;
            }
            ketinggian = tambah - kurang;

            if (ketinggian == 0 && inputRuteArray[i] == 't'){
                countGunung++;
                //System.out.println("gunung");
            }
            else if (ketinggian == 0 && inputRuteArray[i] == 'n') {
                countLembah++;
                //System.out.println("lembah");
            }
        }
        System.out.println("Jumlah gunung = " + countGunung);
        System.out.println("Jumlah lembah = " + countLembah);

    }
}
