package Pretest;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan kata");
        String inputKalimat = input.nextLine();

        String kalimat = inputKalimat.trim().toLowerCase();

        //String alfabet = "abcdefghijklmnopqrstuvwxyz";

        char[] inputArray = kalimat.toCharArray();

        char[] vokal = {'a','i','u','e','o'};

        char[] konsonan = {'b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z'};

        for (int i = 0; i < vokal.length; i++) {
            for (int j = 0; j < inputArray.length; j++) {
                if(vokal[i] == inputArray[j]){
                        System.out.print(vokal[i] + " ");
                }
            }
        }

        System.out.println("");

        for (int i = 0; i < konsonan.length; i++) {
            for (int j = 0; j < inputArray.length; j++) {
                if(konsonan[i] == inputArray[j]){
                    System.out.print(konsonan[i] + " ");
                }
            }
        }

    }
}
