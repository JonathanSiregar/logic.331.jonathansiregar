package Pretest;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;
        boolean flag  = true;
        String answer = "y";

        while (flag){

            System.out.println("Pilihan soal (1 - 10)");
            pilihan = input.nextInt();

            while(pilihan < 1 || pilihan > 10){
                System.out.println("Angka tidak tersedia");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1 :
                   Soal01.Resolve();
                    break;
                case  2 :
                    Soal02.Resolve();
                    break;
                case 3 :
                    Soal03.Resolve();
                    break;
                case 4 :
                    Soal04.Resolve();
                    break;
                case 5 :
                    Soal05.Resolve();
                    break;
                case 6 :
                   //Soal05.Resolve();
                    break;
                case 7 :
                    //No07MedianMeanModusBelumSelesai.Resolve();
                    break;
                case 8 :
                    Soal08.Resolve();
                    break;
                case 9 :
                    Soal09.Resolve();
                    break;
                case 10 :
                    //No10Cupcake.Resolve();
                   break;
            }

//            System.out.println("Try again? y/n");
//            input.nextLine(); //skip bug
//            answer = input.nextLine();

            if (!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }
    }
}